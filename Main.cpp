/* 
 * File:   Main.cpp
 * Author: Darren Silke
 *
 * Created on 20 April 2015, 7:48 AM
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cstdlib>

#include "Image.h"

using std::cerr;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::vector;
using std::stringstream;

using namespace SLKDAR001;

/*
 * Function declaration to read in a PGM image file.
 */
Image readImage(const string & fileName);
/*
 * Function declaration to create and write to a PGM image file.
 */
void writeImage(const string & fileName, Image & result);

/*
 * Main driver function.
 */
int main(int argc, char* argv[]) 
{       
    if(argc == 5)
    {
        if(string(argv[1]) == "-a")
        {
            // -a l1 l2 (add l1 and l2).
            Image image1 = readImage(string(argv[2]));
            Image image2 = readImage(string(argv[3]));
            Image result = image1 + image2;
            writeImage(string(argv[4]), result);
        }
        else if(string(argv[1]) == "-s")
        {
            // -s l1 l2 (subtract l2 from l1).
            Image image1 = readImage(string(argv[2]));
            Image image2 = readImage(string(argv[3]));
            Image result = image1 - image2;
            writeImage(string(argv[4]), result);
        }
        else if(string(argv[1]) == "-l")
        {
            // -l l1 l2 (mask l1 with l2).
            Image image1 = readImage(string(argv[2]));
            Image image2 = readImage(string(argv[3]));
            Image result = image1 / image2;
            writeImage(string(argv[4]), result);
        }
        else if(string(argv[1]) == "-t")
        {
            // -t l1 f (threshold l1 with integer value f).
            Image image = readImage(string(argv[2]));
            int threshold = atoi(argv[3]);
            Image result = image * threshold;
            writeImage(string(argv[4]), result);
        }
        else
        {
            // Invalid option.
            std::cout << "Invalid option.\n"
                      << "Please refer to the assignment instructions or the README file for details on\n"
                      << "how to specify the arguments for this application.\n";
        }
    }
    else if (argc == 4)
    {
        if(string(argv[1]) == "-i")
        {
            // -i l1 (invert l1).
            Image image = readImage(string(argv[2]));
            Image result = !image;
            writeImage(string(argv[3]), result);
        }
        else
        {
            // Invalid option.
            std::cout << "Invalid option.\n"
                      << "Please refer to the assignment instructions or the README file for details on\n"
                      << "how to specify the arguments for this application.\n";
        }
    }
    else
    {
        // Invalid command line arguments.       
        std::cout << "Invalid command line arguments.\n"
                  << "Please refer to the assignment instructions or the README file for details on\n"
                  << "how to specify the arguments for this application.\n";
    } 
    
    return 0;
}

/*
 * Function reads in a PGM image file.
 */
Image readImage(const string & fileName)
{
    ifstream inStream(fileName.c_str(), ios::in | ios:: binary); 
    if(inStream.fail())
    {
        cerr << "Could not open " << fileName << " for reading.\n";
        exit(1);
    }
      
    // Read in header.
    // The header should be P5.
    string header;
    getline(inStream, header);
    
    // Read in comment lines.
    string line;
    vector<string> comments;
    bool exitLoop = false;
    
    getline(inStream, line);
    while(exitLoop == false)
    {
        if(line[0] != '#')
        {
            exitLoop = true;
        }
        else
        {
            comments.push_back(line);
            getline(inStream, line);
        }
    }
    
    // Read in the width and height of the image.
    stringstream ss;
    ss << line;
    int width;
    int height;
    ss >> width >> height;
    
    // Read in the maximum intensity value.
    // The value should be 255.
    int maxIntensity;
    getline(inStream, line);
    maxIntensity = atoi(line.c_str());
    
    unsigned char* data = new unsigned char[width * height];
    
    // While there is still stuff to read...
    while(!inStream.eof())
    {
        // Read in the binary data block.
        inStream.read(reinterpret_cast<char *>(data), width * height);
    }
    inStream.close();    
    Image image(width, height, data);
    delete[] data;
    
    return image;
}

/*
 * Function creates and writes to a PGM image file.
 */
void writeImage(const string & fileName, Image & result)
{
    ofstream outStream(fileName.c_str(), ios::out | ios::binary);
    if(outStream.fail())
    {
        cerr << "Could not open " << fileName << " for writing.\n";
        exit(1);
    }
    
    // Write text header information to outStream.
    outStream << "P5" << endl;
    outStream << "# Image created using the C++ programming language." << endl;
    outStream << "# Image created by Darren Silke." << endl;
    outStream << result.getWidth() << " " << result.getHeight() << endl;
    outStream << "255" << endl;
    
    // Write binary data to outStream.
    outStream.write(reinterpret_cast<char *>(result.getData()), result.getWidth() * result.getHeight());
    outStream.close();
}
