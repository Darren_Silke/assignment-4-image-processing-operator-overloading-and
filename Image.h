/* 
 * File:   Image.h
 * Author: Darren Silke
 *
 * Created on 20 April 2015, 8:53 AM
 */

#ifndef IMAGE_H
#define	IMAGE_H

#include <memory>
#include <iostream>

using std::unique_ptr;
using std::istream;
using std::ostream;

namespace SLKDAR001
{
    // Image class to perform various operations on images.
    class Image
    {
        /*
         * Overloaded output operator for Image.
         */
        friend ostream& operator << (ostream & s, const Image & rhs);
        /*
         * Overloaded input operator for Image.
         */
        friend istream& operator >> (istream & s, Image & rhs);
        
    private:
        
        int width, height;
        unique_ptr<unsigned char[]> data;
        
    public:

        /*
         * Default constructor.
         */
        Image() 
        : width(0), height(0), data(nullptr)
        {
            /*
             * Deliberately left empty.
             */
        }
        
        /*
         * Parameterized constructor.
         */
        Image(int newWidth, int newHeight, unsigned char* newData)
        : width(newWidth), height(newHeight)
        {
            data.reset(new unsigned char[width * height]);
            for(int i = 0; i < width * height; i++)
            {
                data[i] = newData[i];
            }
        }
        
        /*
         * Destructor.
         */
        ~Image()
        {
            if((width != -1) && (height != -1))
            {
                data.reset(nullptr);
            }
        }
        
        /*
         * Copy constructor.
         */
        Image(const Image & rhs) 
        : width(rhs.width), height(rhs.height)
        {
            data.reset(new unsigned char[width * height]);
            for(int i = 0; i < width * height; i++)
            {
                data[i] = rhs.data[i];
            }
        }

        /*
         * Move constructor.
         */
        Image(Image && rhs) 
        : width(std::move(rhs.width)), height(std::move(rhs.height)), data(std::move(rhs.data))
        {
            rhs.width = -1;
            rhs.height = -1;
            rhs.data.reset(nullptr);
        }
        
        /*
         * Copy assignment operator.
         */
        Image & operator = (const Image & rhs)
        {
            if(this != &rhs)
            {
                // Release current resources.
                if((width != -1) && (height != -1))
                {
                    data.reset(nullptr);
                }
                // Copy data across.
                width = rhs.width;
                height = rhs.height;
                
                data.reset(new unsigned char[width * height]);
                for(int i = 0; i < width * height; i++)
                {
                    data[i] = rhs.data[i];
                }
            }
            return *this;
        }
        
        /*
         * Move assignment operator.
         */
        Image & operator = (Image && rhs)
        {
            if(this != &rhs)
            {
                // Release current resources.
                if((width != -1) && (height != -1))
                {
                    data.reset(nullptr);
                }
                // Steal resources/values from rhs!
                width = rhs.width;
                height = rhs.height;
                data = std::move(rhs.data);
                // Leave rhs in empty state.
                rhs.width = -1;
                rhs.height = -1;
                rhs.data.reset(nullptr);
            }
            return *this;
        }
        
        // Nested iterator class.
        class iterator
        {
            
        private:
            
            unsigned char *ptr;
            
            // Grant private access to Image class functions.
            friend class Image;
            
            /*
             * Parameterized constructor.
             * Construct only via Image class (begin/end).
             */
            iterator(u_char *p) : ptr(p)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
        public:
            
            /*
             * Copy constructor.
             * Must be public.
             */
            iterator(const iterator & rhs) : ptr(rhs.ptr)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            
            /*
             * Copy assignment operator.
             */
            iterator & operator = (const iterator & rhs)
            {
                ptr = rhs.ptr;
                return *this;
            }
            
            /*
             * Operator ++ (prefix form).
             */
            iterator & operator ++ () 
            {
                ++ptr;
                return *this;
            }
            
            /*
             * Operator -- (prefix form).
             */
            iterator & operator -- ()
            {
                --ptr;
                return *this;
            }
            
            /*
             * Operator *
             */
            u_char operator * ()
            {
                return *ptr;
            }
            
            /*
             * Operator !=
             */
            bool operator != (const iterator & rhs)
            {
                return *ptr != *rhs.ptr;
            }
        };
        
        /*
         * Function to set iterator to beginning of data.
         */
        iterator begin();
        /*
         * Function to set iterator to "one-past" end of data.
         */
        iterator end();
        
        /*
         * Overloaded plus operator to add two images together.
         */
        Image operator + (Image & rhs);
        /*
         * Overloaded minus operator to subtract one image from another.
         */
        Image operator - (Image & rhs);
        /*
         * Overloaded negation operator to negate an image.
         */
        Image operator ! ();
        /*
         * Overloaded divide operator to divide one image by another image.
         */
        Image operator / (Image & rhs);
        /*
         * Overloaded times operator to multiply one image by a constant 
         * integer.
         */
        Image operator * (const int threshold);
        
        /*
         * Accessor function to get image width.
         */
        int getWidth();
        /*
         * Accessor function to get image height.
         */
        int getHeight();
        /*
         * Accessor function to get image data.
         */
        unsigned char* getData();
    };
}
#endif	/* IMAGE_H */
