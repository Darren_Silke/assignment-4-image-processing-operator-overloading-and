Author: Darren Silke
Date: 28 April 2015

Name: Assignment 4 - Image Processing (Operator Overloading And Containers)

Description: This software implements an image class which supports simple operations on pairs of PGM images. An image is a 2D array of numbers, each of which corresponds to the intensity of a pixel on the screen. Essentially, the program manipulates 2D arrays of numbers. For any pair of "source" images, the program supports the following image operations, which generate a new output image:

1. Add I1 I2: Add the pixel values of I1 to I2 (i.e. at every corresponding 2D position, the two values are added).

2. Subtract I1 I2: Subtract pixel values of I2 from I1.

3. Invert I1: Replace each pixel value p with (255 - p). Note: This is for one image only.

4. Mask I1 I2: Given I1 and an image I2, copy across values from I1 where I2 has a value of 255. All other output values are set to 0. 

5. Threshold I1 f: For all pixels in I1 > f, set the result to the integer 255, otherwise set the value to 0. One can build a mask using this function.

Instructions:

1. Open terminal BASH.
2. Type 'make' to compile and link all C++ source files.
3. To run the software, see HOW TO RUN below.
4. Follow the instructions that appear on the screen (if any).
5. Evaluate output.
6. To remove the 'imageops' executable and the .o files, type 'make clean'.

HOW TO RUN:

To run the application, make sure that any files needed by the application are in the same directory as the application. In this case, ensure that all .PGM files used by the application are in the same directory as the application.

To invoke the application on the terminal, the following format is required, as the application relies on command line arguments:

imageops <option> OutputImageName

where imageops is the name of the executable, <option> represents the single operation to be performed and OutputImageName is the name for the result image. Note that only one option should be specified when the application is run.

BELOW ARE SOME EXAMPLES OF INVOKING THE APPLICATION FOR EACH OF THE FIVE OPERATIONS MENTIONED ABOVE USING THE PGM FILES, Lenna_standard.pgm AND Lenna_hat_mask.pgm AND result.pgm FOR THE OUTPUT FILE.

1. imageops -a Lenna_standard.pgm Lenna_hat_mask.pgm result.pgm
2. imageops -s Lenna_standard.pgm Lenna_hat_mask.pgm result.pgm
3. imageops -i Lenna_standard.pgm result.pgm
4. imageops -l Lenna_standard.pgm Lenna_hat_mask.pgm result.pgm
5. imageops -t Lenna_standard.pgm 50 result.pgm

NOTE: For 5 above, 50 represents an integer value, f.

Please refer to the assignment instructions for more information.

List Of Files:

1. README.txt - Information file.
2. Makefile - Used to compile the program.
3. Image.h - Declares the Image class as well as the various functions in the program to be implemented by the Image.cpp file.
4. Image.cpp - Implements the various functions declared in the Image.h file.
5. Main.cpp - Driver file where the main function lies. This file makes use of both the Image.h and Image.cpp files.
6. Lenna_standard.pgm - Serves as an input file (included for convenience sake).
7. Lenna_hat_mask.pgm - Serves as an input file (included for convenience sake).
8. shrek_rectangular.pgm - Serves as an input file (included for convenience sake).
9. donkey_mask.pgm - Serves as an input file (included for convenience sake).

TAKE NOTE OF THE .git FOLDER WHICH CONTAINS ALL INFORMATION RELATING TO THE USE OF GIT FOR A LOCAL REPOSITORY AS REQUIRED BY THIS COURSE.
