/* 
 * File:   Image.h
 * Author: Darren Silke
 *
 * Created on 27 April 2015, 3:48 PM
 */

#include <cstdlib>

#include "Image.h"

using std::cerr;
using std::endl;
using std::ws;

namespace SLKDAR001
{
    // Function to set iterator to beginning of data.
    Image::iterator Image::begin()
    {
        return iterator(data.get());
    }
    
    // Function to set iterator to "one-past" end of data.
    Image::iterator Image::end()
    {
        return iterator(data.get() + (width * height));
    }
    
    // Overloaded plus operator to add two images together.
    Image Image::operator +(Image& rhs)
    {
        if(width * height != rhs.width * rhs.height)
        {
            cerr << "Cannot perform operation on images, because the sizes of the images differ.\n"
                 << "Image 1 is: " << width << " * " << height << endl
                 << "Image 2 is: " << rhs.width << " * " << rhs.height << endl;
            exit(1);
        }
        
        unsigned char* resultData = new unsigned char[width * height];
        
        Image::iterator lhsBegin = this->begin(), lhsEnd = this->end();
        Image::iterator rhsBegin = rhs.begin(), rhsEnd = rhs.end();
              
        /*
         * THIS BLOCK OF CODE SEEMS TO ONLY WORK WITH SQUARE IMAGES. I DON'T 
         * KNOW WHY IT WON'T WORK WITH RECTANGULAR IMAGES.
        int index = 0;
        while((lhsBegin != lhsEnd) || (rhsBegin != rhsEnd))
        {            
            if((*lhsBegin + *rhsBegin >= 0) && (*lhsBegin + *rhsBegin <= 255))
            {            
                resultData[index] = *lhsBegin + *rhsBegin;
            }
            else if(*lhsBegin + *rhsBegin < 0)
            {
                resultData[index] = 0;
            }
            else
            {
                resultData[index] = 255;
            } 

            ++lhsBegin;
            ++rhsBegin; 
            ++index;
        }
        */
        
        for(int i = 0; i < width * height; i++)
        {
            if((data[i] + rhs.data[i] >= 0) && (data[i] + rhs.data[i] <= 255))
            {
                resultData[i] = data[i] + rhs.data[i];
            }
            else if(data[i] + rhs.data[i] < 0)
            {
                resultData[i] = 0;
            }
            else
            {
                resultData[i] = 255;
            }
        }
        
        Image result(width, height, resultData);
        delete[] resultData;
        
        return result;
    }
    
    // Overloaded minus operator to subtract one image from another.
    Image Image::operator - (Image& rhs)
    {
        if(width * height != rhs.width * rhs.height)
        {
            cerr << "Cannot perform operation on images, because the sizes of the images differ.\n"
                 << "Image 1 is: " << width << " * " << height << endl
                 << "Image 2 is: " << rhs.width << " * " << rhs.height << endl;
            exit(1);
        }
        
        unsigned char* resultData = new unsigned char[width * height];
        
        Image::iterator lhsBegin = this->begin(), lhsEnd = this->end();
        Image::iterator rhsBegin = rhs.begin(), rhsEnd = rhs.end();
        
        /*
         * THIS BLOCK OF CODE SEEMS TO ONLY WORK WITH SQUARE IMAGES. I DON'T 
         * KNOW WHY IT WON'T WORK WITH RECTANGULAR IMAGES.
        int index = 0;
        while((lhsBegin != lhsEnd) || (rhsBegin != rhsEnd))
        {            
            if((*lhsBegin - *rhsBegin >= 0) && (*lhsBegin - *rhsBegin <= 255))
            {            
                resultData[index] = *lhsBegin - *rhsBegin;
            }
            else if(*lhsBegin - *rhsBegin < 0)
            {
                resultData[index] = 0;
            }
            else
            {
                resultData[index] = 255;
            }     
            ++lhsBegin;
            ++rhsBegin;
            ++index;
        }
        */
       
        for(int i = 0; i < width * height; i++)
        {
            if((data[i] - rhs.data[i] >= 0) && (data[i] - rhs.data[i] <= 255))
            {
                resultData[i] = data[i] - rhs.data[i];
            }
            else if(data[i] - rhs.data[i] < 0)
            {
                resultData[i] = 0;
            }
            else
            {
                resultData[i] = 255;
            }
        }
        
        Image result(width, height, resultData);
        delete[] resultData;
        
        return result;
    }
    
    // Overloaded negation operator to negate an image.
    Image Image::operator ! ()
    {
        unsigned char* resultData = new unsigned char[width * height];
        Image::iterator lhsBegin = this->begin(), lhsEnd = this->end();
        
        /*
         * THIS BLOCK OF CODE SEEMS TO ONLY WORK WITH SQUARE IMAGES. I DON'T 
         * KNOW WHY IT WON'T WORK WITH RECTANGULAR IMAGES.
        int index = 0;
        while(lhsBegin != lhsEnd)
        {            
            if((255 - *lhsBegin >= 0) && (255 - *lhsBegin <= 255))
            {            
                resultData[index] = 255 - *lhsBegin;
            }
            else if(255 - *lhsBegin < 0)
            {
                resultData[index] = 0;
            }
            else
            {
                resultData[index] = 255;
            }     
            ++lhsBegin;
            ++index;
        }
        */
        
        for(int i = 0; i < width * height; i++)
        {
            if((255 - data[i] >= 0) && (255 - data[i] <= 255))
            {
                resultData[i] = 255 - data[i];
            }
            else if(255 - data[i] < 0)
            {
                resultData[i] = 0;
            }
            else
            {
                resultData[i] = 255;
            }
        }
        
        Image result(width, height, resultData);
        delete[] resultData;
        
        return result;
        
    }
    
    // Overloaded divide operator to divide one image by another image.
    Image Image::operator / (Image& rhs)
    {
        if(width * height != rhs.width * rhs.height)
        {
            cerr << "Cannot perform operation on images, because the sizes of the images differ.\n"
                 << "Image 1 is: " << width << " * " << height << endl
                 << "Image 2 is: " << rhs.width << " * " << rhs.height << endl;
            exit(1);
        }
        
        unsigned char* resultData = new unsigned char[width * height];
        
        Image::iterator lhsBegin = this->begin(), lhsEnd = this->end();
        Image::iterator rhsBegin = rhs.begin(), rhsEnd = rhs.end();
        
        /*
         * THIS BLOCK OF CODE SEEMS TO ONLY WORK WITH SQUARE IMAGES. I DON'T 
         * KNOW WHY IT WON'T WORK WITH RECTANGULAR IMAGES.
        int index = 0;
        while((lhsBegin != lhsEnd) || (rhsBegin != rhsEnd))
        {            
            if(*rhsBegin == 255)
            {
                resultData[index] = *lhsBegin;
            }
            else if(*rhsBegin == 0)
            {
                resultData[index] = 0;
            }       
            ++lhsBegin;
            ++rhsBegin;
            ++index;
        }
        */
        
        for(int i = 0; i < width * height; i++)
        {
            if(rhs.data[i] == 255)
            {
                resultData[i] = data[i];
            }
            else if(rhs.data[i] == 0)
            {
                resultData[i] = 0;
            }
        }
        
        Image result(width, height, resultData);
        delete[] resultData;
        
        return result;
    }
    
    // Overloaded times operator to multiply one image by a constant integer.
    Image Image::operator *(const int threshold)
    {        
        unsigned char* resultData = new unsigned char[width * height];
        
        Image::iterator lhsBegin = this->begin(), lhsEnd = this->end();
        
        /*
         * THIS BLOCK OF CODE SEEMS TO ONLY WORK WITH SQUARE IMAGES. I DON'T 
         * KNOW WHY IT WON'T WORK WITH RECTANGULAR IMAGES.
        int index = 0;
        while(lhsBegin != lhsEnd)
        {             
            if(*lhsBegin > threshold)
            {
                resultData[index] = 255;
            }
            else
            {
                resultData[index] = 0;
            }       
            ++lhsBegin;
            ++index;
        }
        */
        
        for(int i = 0; i < width * height; i++)
        {
            if(data[i] > threshold)
            {
                resultData[i] = 255;
            }
            else
            {
                resultData[i] = 0;
            }         
        }
        
        Image result(width, height, resultData);
        delete[] resultData;
        
        return result;
    }
    
    // Overloaded output operator for Image.
    ostream& operator << (ostream & s, const Image & rhs)
    {
        s << rhs.width << endl;
        s << rhs.height << endl;
        
        for(int i = 0; i < rhs.width * rhs.height; i++)
        {    
            s.write(reinterpret_cast<char *>(rhs.data.get()), rhs.width * rhs.height);
        }
        return s;
    }
    
    // Overloaded input operator for Image.
    istream& operator >> (istream & s, Image & rhs)
    {              
        s >> rhs.width >> ws;
        s >> rhs.height >> ws;
        s.read(reinterpret_cast<char *>(rhs.data.get()), rhs.width * rhs.height);
        
        return s;
    }
    
    // Accessor function to get image width.
    int Image::getWidth()
    {
        return width;
    }
    
    // Accessor function to get image height.
    int Image::getHeight()
    {
        return height;
    }
    
    // Accessor function to get image data.
    unsigned char* Image::getData()
    {
        return data.get();
    }
}
