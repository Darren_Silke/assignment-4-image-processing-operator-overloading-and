# Makefile for compiling C++ source code.
# Darren Silke (SLKDAR001), 2015

CC=g++ # The compiler name.
CCFLAGS=-std=c++11 # Flags passed to compiler.

# The normal build rules.

imageops: Main.o Image.o
	$(CC) $(CCFLAGS) Main.o Image.o -o imageops

Main.o: Main.cpp
	$(CC) $(CCFLAGS) Main.cpp -c

Image.o: Image.cpp Image.h
	$(CC) $(CCFLAGS) Image.cpp -c

# Clean rule.

clean:

	rm -f *.o imageops
